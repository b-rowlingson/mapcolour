# mapcolour


## map colouring via the greedy algorithm

install `spdep` and `devtools`. Then:

`devtools::install_git("https://gitlab.com/b-rowlingson/mapcolour.git")` 

should install it for you. There's one function, `nacol`, that 
takes a single spatial polygons data frame as an argument and
returns a vector of indices you can use to colour the map.
